import { Component } from '@angular/core';

import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ActionTypes, CounterAction } from './counter.actions';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  count$: Observable<number>;

  constructor(
    private store: Store<{ count: number }>
  ) {
    this.count$ = store.pipe(select('count'));
  }

  increment(key) {
    this.store.dispatch(new CounterAction(ActionTypes.Increment, key));
  }

  decrement(key) {
    this.store.dispatch(new CounterAction(ActionTypes.Decrement, key));
  }

  reset(key) {
    this.store.dispatch(new CounterAction(ActionTypes.Reset, key));
  }

}
