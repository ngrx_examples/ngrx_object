import { Action } from '@ngrx/store';


export enum ActionTypes {
  Increment = 'Increment',
  Decrement = 'Decrement',
  Reset = 'Reset',
}

export class CounterAction implements Action {
  constructor(public type: ActionTypes, public payload: string) {}
}
