import { ActionTypes, CounterAction } from './counter.actions';

export const initialState = {
  first: 0,
  second: 0,
  third: 0
};

export function counterReducer(state = initialState, action: CounterAction) {
  switch (action.type) {
    case ActionTypes.Increment:
      return {
        ...state,
        [action.payload]: state[action.payload] + 1,
      };

    case ActionTypes.Decrement:
      return {
        ...state,
        [action.payload]: state[action.payload] - 1,
      };

    case ActionTypes.Reset:
      return {
        ...state,
        [action.payload]: 0,
      };

    default:
      return state;
  }
}


